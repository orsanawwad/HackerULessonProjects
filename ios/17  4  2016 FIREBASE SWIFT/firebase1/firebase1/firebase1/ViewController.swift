//
//  ViewController.swift
//  firebase1
//
//  Created by HackerU on 17/04/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit
import  Firebase
class ViewController: UIViewController {

    var url = "https://iostest11.firebaseio.com/"
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
       add_person_by_path("ahmad", id1: "1111", adress: "beleen")
        add_person_by_path("lior", id1: "555", adress: "naharia")
           add_person_by_path("mosafa", id1: "888", adress: "fahem")
        
        
        get_data2()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func add_person(name:String,id1:String,adress:String)
    {
        var fbs1: Firebase
        fbs1 = Firebase(url: url)
        var p1 : person
        p1 = person(name: name, id1: id1, adress: adress)
        fbs1.childByAutoId().setValue(p1.getDictionary())

    }
    
    func get_data1()
    {
        var fbs : Firebase
        fbs = Firebase(url: url)
       
  
        
        
        fbs.observeEventType(.Value, withBlock: { snapshot in
            
            
             var name,id1,adress: String
            for item in snapshot.children
            {
                name = item.value!!["name"]  as! String
                   id1 = item.value!!["id1"]  as! String
                   adress = item.value!!["adress"]  as! String
                
                               NSLog("  %@   %@  %@", name,id1,adress)
                var p11: person
                p11 = person(item: item as! FDataSnapshot)
                
            }
            
            
        })
        
        
    }
    
    
    func add_person_by_path(name:String,id1:String,adress:String)
    {
        var fbs1: Firebase
        
        fbs1 = Firebase(url: url)
        var p1 : person
        p1 = person(name: name, id1: id1, adress: adress)
      //  fbs1.childByAutoId().setValue(p1.getDictionary())
        fbs1.childByAppendingPath("ioeal").childByAutoId().setValue(p1.getDictionary())
        
    }

    func get_data2()
    {
        var fbs : Firebase
        fbs = Firebase(url: url)
        
        
        
        
        fbs.childByAppendingPath("ioeal").observeEventType(.Value, withBlock: { snapshot in
            
            
            var name,id1,adress: String
            for item in snapshot.children
            {
                name = item.value!!["name"]  as! String
                id1 = item.value!!["id1"]  as! String
                adress = item.value!!["adress"]  as! String
                
                NSLog("  %@   %@  %@", name,id1,adress)
                var p11: person
                p11 = person(item: item as! FDataSnapshot)
                
            }
            
            
        })
        
        
    }

    

}

