//
//  my_view.swift
//  graphices_2
//
//  Created by HackerU on 24/02/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit

class my_view: UIView {

    
    var l1  = CGPoint(x: 0,y: 0)
    var flag : Int16 = 0
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        
        let context = UIGraphicsGetCurrentContext()
        var color = UIColor.greenColor().CGColor
        CGContextSetLineWidth(context, 5.0)
        CGContextSetStrokeColorWithColor(context, color)
        CGContextMoveToPoint(context, 100, 100)
        CGContextAddLineToPoint(context, 300, 400)
      
        if flag == 1
        {
            CGContextAddLineToPoint(context, 150, 200)
        }
        
        if flag == 2
        {
            var r1 = CGRectMake(200, 200, 200, 300)
            CGContextAddRect(context, r1)
        }
        
        CGContextAddLineToPoint(context, l1.x, l1.y)
        
          CGContextStrokePath(context)
        
        // Drawing code
    }
   
    func draw(flag : Int16  )
    {
      self.flag = flag
        setNeedsDisplay()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        let touch = touches.first! as UITouch
   l1 =      touch.locationInView(self)
       setNeedsDisplay()
        
    }

}
