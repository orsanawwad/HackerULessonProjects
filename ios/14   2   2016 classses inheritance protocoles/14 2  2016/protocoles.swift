//
//  main.swift
//  protocols1
//
//  Created by HackerU on 14/02/2016.
//  Copyright © 2016 HackerU. All rights reserved.
//

import Foundation

print("Hello, World!")


protocol pr1
{
    func f1()
    func f2(x : Int16, y :Int16) -> Int16
}

protocol pr2
{
    func f3()
    func f4(x : Int16, y :Int16) -> Int16
    
    var str: String{set get}
}


class base1:  pr1,pr2 {
    
    
    // base class must implements the set anf get
    
    
    func f3()
    {
      
    }
    func f4(x: Int16, y: Int16) -> Int16 {
        return x+y
    }
    
    func f1() {
        
    }
    func f2(x: Int16, y: Int16) -> Int16 {
        return x+y
    }
    
   }


class class5 {
    static func f1()
    {
        
    }
}
class5.f1()



class class3 {
    func f1()
    {
        
    }
}
class class4: class3,pr1,pr1 {
   
}
struct stu3: pr1 {
  
}







