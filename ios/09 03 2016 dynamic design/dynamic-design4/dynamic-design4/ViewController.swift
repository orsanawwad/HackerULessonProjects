//
//  ViewController.swift
//  dynamic-design4
//
//  Created by HackerU on 09/03/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var mv1 : my_view?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func add_view(sender: AnyObject) {
        var r3 = CGRectMake(50, 20, 200, 200)
        mv1 = my_view(frame: r3)
        self.view.addSubview(mv1!)
    }

}

