//
//  my_view.swift
//  dynamic5
//
//  Created by HackerU on 09/03/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit

class my_view: UIView {

    
    var my_slider: UISlider?
    var my_switcher : UISwitch?
    var textFeled : UITextField?
    var b1 : UIButton?
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        var r1 = CGRectMake(0, 0, 30, 30)
        my_switcher = UISwitch(frame: r1)
        my_switcher?.on = true
        my_switcher?.setOn(true, animated: true)
        self.addSubview(my_switcher!)
        var r2 = CGRectMake(0, 50, 50, 50)
        textFeled = UITextField(frame: r2)
        textFeled?.text = "first text "
       // textFeled?.font?.fontName = "meriam"
        textFeled?.font?.fontWithSize(24.0)
        textFeled?.textColor = UIColor.blackColor()
        self.addSubview(textFeled!)
        var r3 = CGRectMake(0, 100, 40, 40)
        b1 = UIButton(frame: r3)

    b1?.setTitle("buttob1", forState: .Normal)
     b1?.addTarget(self, action: "f1:", forControlEvents: .TouchUpInside)
        b1?.backgroundColor =  UIColor.greenColor() 
    self.addSubview(b1!)
    
    }
    
    @IBAction func f1(sender : UIButton!)
    {
        textFeled?.text = "button clicked"
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

}
