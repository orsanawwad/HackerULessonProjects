//
//  ViewController.swift
//  Calculator_tar_2
//
//  Created by HackerU on 17/02/2016.
//  Copyright © 2016 yoelnet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var flag = false
    var temp:Int16 = 0
    var action:String = ""
    var num:Int16 = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        ResultField.text = "0"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBOutlet weak var ResultField: UITextField!
    
    @IBAction func numbersBut(sender: AnyObject) {
        
        var pushBut: UIButton
        pushBut = (sender as! UIButton)
        num = Int16.init(ResultField.text!)!
        if flag == true
        {
            num = 0
            flag = false
        }
        num = num * 10 + Int16.init(pushBut.titleLabel!.text!)!
        ResultField.text = "\(num)"


        
        
        
    }
    
    
    @IBAction func actionsBut(sender: AnyObject) {
        var actBut: UIButton
        actBut = (sender as! UIButton)
        if flag != true
        {
        flag = true
        num = Int16.init(ResultField.text!)!
        switch action
        {
        case "+":
            temp += num
            break
            
        case "-":
            temp -= num
            break

        case "/":
            temp /= num
            break

        case "*":
            temp *= num
            break
        
            
        default:
            temp = num
        }
        action = actBut.titleLabel!.text!
        ResultField.text = "\(temp)"
        }

    }
}

