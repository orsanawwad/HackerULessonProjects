//
//  persons.swift
//  tableViewWithPics
//
//  Created by HackerU on 06/03/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import Foundation
import UIKit
class person {
    var name: String?
    var adress : String?
    var image : UIImage?
    
    init(name : String,adress : String,im : UIImage)
    {
        self.name = name
        self.adress = adress
        self.image = im
    }
    
}
