//
//  ViewController.swift
//  tableViewWithPics
//
//  Created by HackerU on 06/03/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    
    var array1 = [person]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        var name,adress,image_name : String
        var index : Int16
        var image1 : UIImage
        var per1 : person
        for index = 1; index < 5 ; index++
        {
            name = "abcd"
            adress = "haifa"
            image_name = "p\(index)"
            image1 = UIImage(named: image_name)!
            per1 = person(name: name, adress: adress, im: image1)
            array1.append(per1)
            
            
        }
    }

    @IBOutlet weak var image: UITextField!
    
    @IBOutlet weak var per_name: UITextField!
    @IBAction func add_person(sender: AnyObject) {
        
        var per11 : person
        var im11 : UIImage
        var im_name : String
        im_name = ed_im.text!
        im11     =  UIImage(named: im_name)!
        per11 = person(name:per_name.text!, adress: per_adress.text!, im: im11)
        
        array1.append(per11)
        t1.reloadData()

        
    }
    
    @IBOutlet weak var t1: UITableView!
    @IBOutlet weak var ed_im: UITextField!
    @IBOutlet weak var per_adress: UITextField!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return  array1.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell : UITableViewCell
        cell = tableView.dequeueReusableCellWithIdentifier("t11", forIndexPath: indexPath) as UITableViewCell
        cell.textLabel?.text = array1[indexPath.row].adress
        cell.detailTextLabel?.text = array1[indexPath.row].name
        cell.imageView?.image = array1[indexPath.row].image
        
        
        return cell
        
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        ed_im.text = array1[indexPath.row].adress
    }

}


