//
//  my_second.swift
//  multy_view_with_casting
//
//  Created by HackerU on 21/02/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit

class my_second: UIViewController {

    var st2 : String?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var label1: UILabel!
    @IBAction func btn_show_data(sender: AnyObject) {
        
        
        label1.text = st2
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
