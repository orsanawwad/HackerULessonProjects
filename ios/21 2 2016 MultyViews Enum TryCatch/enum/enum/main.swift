//
//  main.swift
//  enum
//
//  Created by HackerU on 21/02/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import Foundation

print("Hello, World!")



enum days
{
    case sunday
    case monday
    case teuseday
    case wednesday
}

let theday = days.monday

var x : days

enum grade_error  : ErrorType
{
    case up_than_100
    case les_than_zero
}

func f1( num : Int16) throws
{
    if num > 100
    {
        throw grade_error.up_than_100
    }
    if num < 0
    {
        throw grade_error.les_than_zero
    }
    // some action with num
    
}

func f2(num : Int16) throws
{
    do{
        defer
            {
            
        }
        
        try f1(120)
        
       
    }
    catch grade_error.les_than_zero
    {
        print("less than 100")
    }
    catch grade_error.up_than_100
    {
        print("up than 100")
    }
    catch
    {
        
    }
}

try f2(120)



















