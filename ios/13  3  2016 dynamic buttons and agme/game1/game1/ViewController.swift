//
//  ViewController.swift
//  game1
//
//  Created by HackerU on 13/03/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var btn2 = [UIButton]()
    var ln1,ln2,lop1: UILabel?
    var operation : Int16?
    
    var n1,n2,tagres : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        var v1 : UIView
        var r1,r2,r3 : CGRect
        var r5,r6,r7,r8 : CGRect
        var index : Int
        for index = 0 ; index < 4 ; index++
        {
            var b1 : UIButton
            r1 = CGRectMake( CGFloat(index * 60 ), 50, 50, 50)
            b1 = UIButton(frame: r1)
            b1 .tag = index
                 b1.backgroundColor = UIColor.greenColor()
            b1.addTarget(self, action: "btn_action:", forControlEvents: .TouchUpInside)
            btn2.append(b1)
            self.view.addSubview(b1)
            
        }
        
        var res = get_result(1)
        sranddev()
        var res_btn = random()  % 4
     tagres =  res_btn
       btn2[res_btn].setTitle("\(res)", forState: .Normal)
        
        for index = 0 ; index < 4 ; index++
        {
            if index != res_btn
            {
                var x : Int
             x = index + res + Int (1)
                btn2[ index].setTitle(  "\(x)", forState: .Normal)
            }
        }
        
        
        
        var r4 = CGRectMake( 50, 150, 300, 50)
        v1 = UIView(frame: r4)
        v1.backgroundColor = UIColor.greenColor()
        r5 = CGRectMake(0, 0, 50, 50)
        ln1  = UILabel(frame: r5)
        var n11,n22 : Int
        n11 = n1!
        ln1?.text = "\(n11)"
       
        
        r5 = CGRectMake(120, 0, 50, 50)
        ln2  = UILabel(frame: r5)
        n22 = n2!
        ln2?.text = "\(n22)"
        //label1.text = "\(n11)"
        r5 = CGRectMake(60, 0, 50, 50)
        lop1  = UILabel(frame: r5)
        
        if operation    == 1
        {
        lop1?.text = "+"
        }
        else
        if operation == 2
        {
             lop1?.text = "-"
        }
else
        {
            lop1?.text = "*"

        }
        v1.addSubview(lop1!)
        v1.addSubview(ln1!)
        v1.addSubview(ln2!)
        self.view.addSubview(v1)

        
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func get_operation()->Int16
    {
        var oper : Int16
        oper = random()%3+1
        return oper
        
    }
    func     get_number(level:Int)->Int
    {
        var res :Int
        var tor : Int
        tor =   Int(level) * Int (10)
        res = random() % tor
        
        
        return res
    }

    @IBOutlet weak var label1: UILabel!
    func get_result(level : Int)->Int
    {
        var n3: Int
        n1 = get_number(level)
        n2 = get_number(level)
        operation = get_operation()
        if operation == 1
        {
            n3 = n1! + n2!
        }
        else
            if operation == 2
        {
            n3 = n1! - n2!
            
        }
        else
            {
        n3 = n1! * n2!
        }
        
        return n3
        
    }
    
    
    @IBAction  func  btn_action(sender : UIButton! )
    {
        if sender.tag == tagres
        {
            // add points
            label1.text = "add points"
        }
        else
        {
            // less points
            label1.text = "less points"
        }
        reload()
    }
    
    
    func reload ()
    {
        var index : Int
        var res = get_result(2)
        sranddev()
        var res_btn = random()  % 4
        tagres =  res_btn
        btn2[res_btn].setTitle("\(res)", forState: .Normal)
        
        for index = 0 ; index < 4 ; index++
        {
            if index != res_btn
            {
                var x : Int
                x = index + res + Int (1)
                btn2[ index].setTitle(  "\(x)", forState: .Normal)
            }
        }

        
        
        var n11,n22 : Int
        n11 = n1!
        ln1?.text = "\(n11)"
        
        n22 = n2!
        ln2?.text = "\(n22)"
       // label1.text = "\(n11)"
        
        if operation    == 1
        {
            lop1?.text = "+"
        }
        else
            if operation == 2
            {
                lop1?.text = "-"
            }
            else
            {
                lop1?.text = "*"
                
        }


        
    }
    
    
    

}

