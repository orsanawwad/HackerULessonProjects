//
//  my_view.swift
//  dynamic_view_3
//
//  Created by HackerU on 13/03/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit

class my_view: UIView {

    
    
    var b1: UIButton?
    var l1 : UILabel?
    
    override     init(frame: CGRect) {
       super.init(frame: frame)
        var r1 = CGRectMake(0, 0, 100,100)
        b1 = UIButton(frame: r1)
        b1?.backgroundColor = UIColor.greenColor()
        b1?.setTitle("button1", forState: .Normal)
        self.addSubview(b1!)
        var r2 = CGRectMake(110, 100, 40, 40)
        l1 = UILabel(frame: r2)
        
        l1?.text = "first label"
        l1?.backgroundColor = UIColor.redColor()
        self.addSubview(l1!)
        self.backgroundColor = UIColor.yellowColor()
        
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
