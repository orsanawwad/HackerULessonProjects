//
//  ViewController.swift
//  picker2
//
//  Created by HackerU on 16/03/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {
    
    var names = ["aaa","bbb","ccc","eeee"]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.picker1.dataSource  = self
        self.picker1.delegate  = self
        var m1 : UIButton
        m1.ge
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBOutlet weak var text1: UITextField!
    @IBAction func btm_action(sender: AnyObject) {
        
        names.append(text1.text!)
        picker1.reloadAllComponents()
    }
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var picker1: UIPickerView!
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        
        return names.count
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return names[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
       label1.text = names[row]
        
    }

}

