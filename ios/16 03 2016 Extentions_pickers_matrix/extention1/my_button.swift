//
//  my_button.swift
//  extention1
//
//  Created by HackerU on 16/03/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import Foundation

import UIKit

extension UIButton{
    
    
    func get_val()->Int
    {
        return self.tag * 10
    }
}