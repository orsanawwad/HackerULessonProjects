//
//  ViewController.swift
//  num_of_section
//
//  Created by HackerU on 02/03/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit

class ViewController: UIViewController ,UITableViewDataSource {

    var names = ["aaa","bbb","ccc","ddd"]
    var adresses = ["haifa","tel-avev","carmeal"]
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0
        {
            return names.count
        }
        else
       
        {
            return adresses.count
        }
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell :UITableViewCell
        cell = UITableViewCell()
        if indexPath.section     == 0
        {
           
            cell.textLabel?.text = names[ indexPath.row]
        }
        else
        if indexPath.section == 1
        {
            cell.textLabel?.text = adresses[ indexPath.row]
            
        }
        return cell
    }

}

