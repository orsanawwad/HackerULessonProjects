//
//  ViewController.swift
//  segment1
//
//  Created by HackerU on 09/03/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBOutlet weak var label1: UILabel!
    @IBAction func segment_changed(sender: UISegmentedControl) {
        
        
        label1.text = "\(sender .selectedSegmentIndex)"
        
    }

}

