package com19.example.hackeru.firebase3_boker;

import java.util.PriorityQueue;

/**
 * Created by hackeru on 4/13/2016.
 */
public class contacts {
    private String name;
private  int age;
    private String adress;

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "contacts{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", adress='" + adress + '\'' +
                '}'+"\n";
    }

    public void setAge(int age) {
        this.age = age;
    }

    public contacts()
    {

    }

    public contacts(String adress, String name,int age) {
        this.adress = adress;

        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }


}
