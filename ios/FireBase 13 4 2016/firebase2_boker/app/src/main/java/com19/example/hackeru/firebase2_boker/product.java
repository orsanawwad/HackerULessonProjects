package com19.example.hackeru.firebase2_boker;

import java.util.PriorityQueue;

/**
 * Created by hackeru on 4/13/2016.
 */
public class product {

    private  String name1;
    private  int price;
    private  String id1;

    @Override
    public String toString() {
        return "product{" +
                "name1='" + name1 + '\'' +
                ", price=" + price +
                ", id1='" + id1 + '\'' +
                '}'+"\n";
    }

    public product(String name1, int price, String id1) {
        this.name1 = name1;
        this.price = price;
        this.id1 = id1;
    }
    public product()
    {

    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getId1() {
        return id1;
    }

    public void setId1(String id1) {
        this.id1 = id1;
    }
}
