package com19.example.hackeru.firebase2_boker;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.firebase.client.core.operation.ListenComplete;
import com.firebase.client.core.view.View;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity  implements android.view.View.OnClickListener{

    Firebase fbs1;
    EditText ed_name,ed_id,ed_price;
    Button b1 ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Firebase.setAndroidContext(this);


        b1 = (Button)findViewById(R.id.btn_add);
        ed_name = (EditText)findViewById(R.id.ed_name);
        ed_id = (EditText)findViewById(R.id.ed_code);
        ed_price = (EditText)findViewById(R.id.ed_price);

b1.setOnClickListener(this);

         fbs1  = new Firebase("https://iostest11.firebaseio.com/");

        fbs1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<product> l1 = new ArrayList<product>();
                for(DataSnapshot dd:dataSnapshot.getChildren())
                {
                    product pr1 = dd.getValue(product.class);
                    l1.add(pr1);
                }
                for(product prr :l1)
                {
                    b1.setText(b1.getText().toString()+prr);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }

    public  void add_product_toFireBase()
    {
        String name,id1;
        int price;
        product pr1;
        name = ed_name.getText().toString();
        id1 = ed_id.getText().toString();
        price = Integer.parseInt(ed_price.getText().toString());
        pr1 = new product(name,price,id1);

        fbs1.push().setValue(pr1);
    }

    @Override
    public void onClick(android.view.View v) {



        add_product_toFireBase();
    }


}
