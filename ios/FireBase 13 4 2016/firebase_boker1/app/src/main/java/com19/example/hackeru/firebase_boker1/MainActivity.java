package com19.example.hackeru.firebase_boker1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ButtonBarLayout;
import android.util.Log;
import android.widget.Button;
import android.widget.EdgeEffect;
import android.widget.EditText;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.firebase.client.core.view.View;

public class MainActivity extends AppCompatActivity   {
    EditText ed1 ;
    Button b1,b2;
    int x=0;
    Firebase fbs1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // once per application
b1 = (Button)findViewById(R.id.button);
        ed1 = (EditText)findViewById(R.id.editText);
        b2 = (Button)findViewById(R.id.button2);
        try {
            person p1 = new person("aaa");
            Firebase.setAndroidContext(this);
             fbs1 = new Firebase("https://iostest11.firebaseio.com/");


            fbs1.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                 //   x++;
                  //  b1.setText("num of changes is "+x);
                    for(DataSnapshot dd:dataSnapshot.getChildren() )
                    {
                        person my_per = dd.getValue(person.class);
                        b2.setText(b2.getText().toString()+my_per.getName());
                    }

                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });

            fbs1.push().setValue(p1);
        } catch (Exception e) {
            Log.i("a1",e.getMessage());
        }


        b1.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                add_person(ed1.getText().toString());
            }
        });
    }

    public void click1(View v1)
    {
        fbs1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
               x++;
              b1.setText("num of changes is "+x);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }
    public void add_person(String name)
    {
        person p11 = new person(name);
        fbs1.push().setValue(p11);
    }
}
