//
//  Entity+CoreDataProperties.swift
//  coreData3_6_4_2016
//
//  Created by HackerU on 06/04/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Entity {

    @NSManaged var name: String?
    @NSManaged var adress: String?
    @NSManaged var age: NSNumber?

}
