//
//  add_view.swift
//  coreData2_6_4_2016
//
//  Created by HackerU on 06/04/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit
import CoreData
class add_view: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func add_person_action(sender: AnyObject) {
        
        var name,id : String
        var age : Int
        var age_to_fun : NSNumber
        name = txf_name.text!
        id = txtf_id.text!
        age = Int(txtf_age.text!)!
        age_to_fun = age
        add_person(name, id: id, age: age_to_fun)
        
        
        
    }
    @IBOutlet weak var txtf_age: UITextField!
    @IBOutlet weak var txtf_id: UITextField!
    @IBOutlet weak var txf_name: UITextField!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func add_person(name: String,id: String,age :NSNumber)
    {
        
        let manageobjectcontxt = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        let entityDescription = NSEntityDescription.entityForName("Entity", inManagedObjectContext: manageobjectcontxt)
        let person = Entity(entity:entityDescription!,insertIntoManagedObjectContext:manageobjectcontxt)
        
        person.name = name
        person.id = id
        person.age = age
        
        do{
            
            try   manageobjectcontxt.save()
        }
        catch
        {
            
        }
    }


}
