//
//  Entity+CoreDataProperties.swift
//  core_data33
//
//  Created by HackerU on 03/04/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Entity {

    @NSManaged var name: String?
    @NSManaged var id1: String?

}
