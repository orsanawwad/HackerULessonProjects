﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace server1.Models
{
    public class car : Controller
    {
        private int number;

        public int Number
        {
            get { return number; }
            set { number = value; }
        }
        private string model;

        public string Model
        {
            get { return model; }
            set { model = value; }
        }
        private string id;

        public string Id
        {
            get { return id; }
            set { id = value; }
        }
        public car(int number, string model, string id)
        {
            this.number = number;
            this.model = model;
            this.id = id;
        }

    }
}
