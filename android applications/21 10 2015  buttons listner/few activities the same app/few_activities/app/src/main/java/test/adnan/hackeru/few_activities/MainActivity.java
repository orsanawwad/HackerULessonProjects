package test.adnan.hackeru.few_activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    int []aa= new int [10];
    Button b1 = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        b1 = (Button)findViewById(R.id.button);
        my_listner m1 = new my_listner();
        for (int i=0;i<aa.length;i++)
            aa[i] = i*i;
        b1.setOnClickListener(m1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class my_listner implements View.OnClickListener
    {

        @Override
        public void onClick(View v) {
            String str = "my user name is adnan";
            int age = 40;
            Intent in1 = new Intent(MainActivity.this,second_activity.class);
            in1.putExtra("user",str);
            in1.putExtra("user_age",age);
            in1.putExtra("my_array",aa);
            startActivity(in1);
        }
    }
}
