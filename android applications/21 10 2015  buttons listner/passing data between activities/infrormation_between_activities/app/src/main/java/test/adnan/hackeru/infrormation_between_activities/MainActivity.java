package test.adnan.hackeru.infrormation_between_activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button add,show;
    public static List<Integer> l1 = new ArrayList<Integer>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        add = (Button)findViewById(R.id.button);
        show = (Button)findViewById(R.id.button2);

        my_listner m1 = new my_listner();
        add.setOnClickListener(m1);
        show.setOnClickListener(m1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public  class  my_listner implements View.OnClickListener
    {

        @Override
        public void onClick(View v) {

            switch (v.getId())
            {
                case R.id.button:

                    Intent in1 = new Intent(MainActivity.this,add_number.class);
                    startActivity(in1);

                    break;
                case R.id.button2:

                    Intent in2 = new Intent(MainActivity.this,show_all.class);
                    startActivity(in2);

                    break;
            }
        }
    }
}
