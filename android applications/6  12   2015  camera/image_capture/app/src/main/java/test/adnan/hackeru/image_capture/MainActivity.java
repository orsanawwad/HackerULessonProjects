package test.adnan.hackeru.image_capture;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    Button b1 = null;
    ImageView im1 = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b1 = (Button)findViewById(R.id.b1);
        im1 = (ImageView)findViewById(R.id.im1);

        my_listner m1 = new my_listner();
        b1.setOnClickListener(m1);




    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        Log.i("request_code",requestCode+"");

        Bundle extra = data.getExtras();
        Bitmap btmp1 = (Bitmap)extra.get("data");
        im1.setImageBitmap(btmp1);



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class  my_listner implements View.OnClickListener
    {

        @Override
        public void onClick(View v) {

            if(!if_have_camera())
            {
                Log.i("test11","no camera");
                return;

            }
            Intent in1 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(in1,1);

        }

        public  boolean if_have_camera()
        {
            return  getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
        }
    }

}
