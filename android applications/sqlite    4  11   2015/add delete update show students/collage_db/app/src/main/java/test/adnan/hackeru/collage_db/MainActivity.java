package test.adnan.hackeru.collage_db;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    student_db sdb1 ;
    Button add,show,show_down,delete,update;

    EditText ed_name,ed_id,ed_avrg,t1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        sdb1 = new student_db(this,"collage",null,1);
        add = (Button)findViewById(R.id.add);

        show = (Button)findViewById(R.id.show);


        delete = (Button)findViewById(R.id.button3);


        update = (Button)findViewById(R.id.button2);
        show = (Button)findViewById(R.id.show);

        show_down = (Button)findViewById(R.id.button);
        ed_name = (EditText)findViewById(R.id.name);
        ed_id = (EditText)findViewById(R.id.id1);
        ed_avrg = (EditText)findViewById(R.id.avrg);
        t1 = (EditText)findViewById(R.id.t1);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                student s1;
                String name,id;
                int aveg;
                name = ed_name.getText().toString();
                id = ed_id.getText().toString();
                aveg = Integer.parseInt(ed_avrg.getText().toString());
                s1 = new student(id,name,aveg);
                sdb1.add_new_student(s1);


            }
        });


        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String all="";
                int num;
                        num = Integer.parseInt(t1.getText().toString());
                List<student> l1 = sdb1.get_by_avrg(num,1);

                for (student st: l1)
                    all+= st+"\n";

show.setText(all);
            }
        });


        show_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String all="";
                int num;
                num = Integer.parseInt(t1.getText().toString());
                List<student> l1 = sdb1.get_by_avrg(num,2);

                for (student st: l1)
                    all+= st+"\n";

                show.setText(all);
            }
        });


        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id;
                int avgg;
                id = ed_id.getText().toString();
                avgg =   Integer.parseInt(ed_avrg.getText().toString());
                sdb1.update_by_id(id,avgg);

            }
        });


        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id;

                id = ed_id.getText().toString();
                sdb1.delete_by_id(id);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
