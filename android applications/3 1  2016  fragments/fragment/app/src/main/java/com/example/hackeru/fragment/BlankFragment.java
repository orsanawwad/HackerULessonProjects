package com.example.hackeru.fragment;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class BlankFragment extends Fragment {


    Button b1 = null;

    public String getMy_str() {
        return my_str;
    }

    public void setMy_str(String my_str) {
        this.my_str = my_str;
    }

    private String my_str= "hello i am the text";
    public BlankFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v1 = inflater.inflate(R.layout.fragment_blank,container,false);
        b1 = (Button)v1.findViewById(R.id.b1_fr1);
        my_listner m1 = new my_listner();
        b1.setOnClickListener(m1);
        return  v1;
    }
class my_listner implements View.OnClickListener
{

    @Override
    public void onClick(View v) {

        b1.setText(my_str);
        MainActivity ma1 = (MainActivity)getActivity();
        ma1.setMessage_from_first_to_swecond_fragment("hello brother i am he first fragmnet man" );
    }
}
}
