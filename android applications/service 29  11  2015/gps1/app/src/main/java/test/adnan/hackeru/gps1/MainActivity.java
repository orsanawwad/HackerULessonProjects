package test.adnan.hackeru.gps1;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements LocationListener{

    LocationManager lm=null;

    Button b1 = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b1 = (Button)findViewById(R.id.b1);
b1.setTextSize(24);
        try {
             lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

            if ( ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {


            }

            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER,2,2,this);

            Location lc = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            b1.setText("latitude " + lc.getLatitude() + " \n longtitude " + lc.getLongitude());
          //  Toast.makeText(getApplicationContext()+lc.getLatitude()+" longtitude "+lc.getLongitude(),Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            b1.setText(e.getMessage());
          Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_LONG).show();
        }

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ( ContextCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {


                }
                Location lc = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                b1.setText("latitude " + lc.getLatitude() + " \n longtitude " + lc.getLongitude());



                try
                {
                    String url = "waze://?ll="+lc.getLatitude()+","+lc.getLongitude()+"&navigate=yes\n";
                    Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse(url) );
                    startActivity( intent );
                }
                catch ( ActivityNotFoundException ex  )
                {
                    Intent intent =
                            new Intent( Intent.ACTION_VIEW, Uri.parse( "market://details?id=com.waze" ) );
                    startActivity(intent);
                }


           //     waze://?ll=<lat>,<lon>&navigate=yes

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
