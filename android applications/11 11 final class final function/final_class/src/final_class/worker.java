package final_class;

public class worker {
	
	private String name;
	private String id;
	private float pay_per_h;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public float getPay_per_h() {
		return pay_per_h;
	}
	public void setPay_per_h(float pay_per_h) {
		this.pay_per_h = pay_per_h;
	}
	public float pay(int h)
	{
		return h*this.pay_per_h;
	}
	public final  float taxes()
	{
		return 0;
	}

}
