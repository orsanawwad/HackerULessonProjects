/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */
package com.parse.starter;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;


public class MainActivity extends ActionBarActivity {
  String all="";
  Button b1,show_all,show_by_id;
  EditText stu_name,st_id,st_avrg;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    ParseAnalytics.trackAppOpenedInBackground(getIntent());

    ParseObject testObject = new ParseObject("table19");
    testObject.put("name", "afaf");
    testObject.saveInBackground();


    my_listner m1 = new my_listner();

    b1 = (Button)findViewById(R.id.save);
    show_all = (Button)findViewById(R.id.show_all);
    show_by_id = (Button)findViewById(R.id.show_by_id);
    st_id = (EditText)findViewById(R.id.stu_id);

    stu_name= (EditText)findViewById(R.id.stu_name);
    st_avrg = (EditText)findViewById(R.id.stu_avrg);
    b1.setOnClickListener(m1);
show_all.setOnClickListener(m1);
    show_by_id.setOnClickListener(m1);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }
  class my_listner implements View.OnClickListener
  {

    @Override
    public void onClick(View v) {
      String name,id;
      int avrg;

      switch (v.getId())
      {
        case R.id.save:
          name = stu_name.getText().toString();
          id = st_id.getText().toString();
          avrg = Integer.parseInt(st_avrg.getText().toString());


          ParseAnalytics.trackAppOpenedInBackgr20ound(getIntent());

          ParseObject testObject = new ParseObject("students_callage");
          testObject.put("name", name);
          testObject.put("stu_id", id);
          testObject.put("avrg", avrg);
          testObject.saveInBackground();
          st_avrg.setText("");
          stu_name.setText("");
          st_id.setText("");
          break;
        case R.id.show_all:

          ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("students_callage");
          query.whereGreaterThan("avrg",90);
          query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {

              if(e==null)
              {
                for (int i=0;i<objects.size();i++)
                {
                  all+= objects.get(i).get("name")+"\n";
                }
                show_all.setText(all);
              }
              else
              {
                Log.i("parse11",e.getMessage());
              }
            }
          });
          break;
        case R.id.show_by_id:
          String STU_IDD;
          STU_IDD = st_id.getText().toString();
          ParseQuery<ParseObject> query1 = new ParseQuery<ParseObject>("students_callage");
          query1.whereEqualTo("stu_id",STU_IDD);
          query1.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {

              if(e==null)
              {
                for (int i=0;i<objects.size();i++)
                {
                  all+= objects.get(i).get("name")+"\n";
                  all+= objects.get(i).get("stu_id")+"\n";
                  all+= objects.get(i).get("avrg")+"\n";
                }
                show_by_id.setText(all);
              }
              else
              {
                Log.i("parse11",e.getMessage());
              }
            }
          });


          break;
      }


    }
  }
}
