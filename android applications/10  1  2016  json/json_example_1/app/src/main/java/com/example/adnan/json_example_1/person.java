package com.example.adnan.json_example_1;

/**
 * Created by adnan on 10/01/2016.
 */
public class person {
    private int age;
    private String id;
    private String name;

    @Override
    public String toString() {
        return "person{" +
                "age=" + age +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    public person(int age, String id, String name) {
        this.age = age;
        this.id = id;
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
