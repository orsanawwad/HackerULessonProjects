package com.example.adnan.json_example_2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class MainActivity extends AppCompatActivity {


    Button b1,b2;
    String all_text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b1 = (Button)findViewById(R.id.b1);
        b2 = (Button)findViewById(R.id.b2);
    }

    public  void my_click(View v1)
    {
        switch (v1.getId())
        {
            case R.id.b1:
                person p_tmp = new person();
                person.adress adr1 = p_tmp.new adress("name of street",100);
                person my_per = new person("1234","sonia",adr1);
                String res;
                res = convert_person_2_json(my_per);
                all_text = res;
                Log.i("a123",res);
                b1.setText(res);
                b1.setTextSize(26);

                break;


            case R.id.b2:

                person result;
                result = convert_from_json_2_person(all_text);
                b2.setText(""+result);
                b2.setTextSize(26);

                break;
        }
    }

    public String convert_person_2_json(person per1)
    {
        String all="";
        JSONObject json_outer = new JSONObject();


        try {
            json_outer.put("name",per1.getName());
            json_outer.put("id",per1.getId());
            JSONObject json_inner = new JSONObject();
            json_inner.put("street_name",per1.getPerson_adress().getStreet_name());
            json_inner.put("street_num",per1.getPerson_adress().getStreet_num());
            json_outer.put("per_adress",json_inner);

            all = json_outer.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return  all;

    }


    public person   convert_from_json_2_person(String res)
    {
    person per11 = null    ;
        String name,id,street;
        int strret_num;
        JSONObject json_outer,jso_inner;


        try {


            json_outer  = new JSONObject(res);
            name = json_outer.getString("name");
            id = json_outer.getString("id");
         jso_inner=   json_outer.getJSONObject("per_adress");
            street = jso_inner.getString("street_name");
            strret_num = jso_inner.getInt("street_num");
            person per14 = new person();
            person.adress adr2 = per14.new adress(street,strret_num);
             per11 = new person(id,name,adr2);




        } catch (JSONException e) {


            e.printStackTrace();
        }


        return per11;
    }


}
