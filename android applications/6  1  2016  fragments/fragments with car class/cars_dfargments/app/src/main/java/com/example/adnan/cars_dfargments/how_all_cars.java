package com.example.adnan.cars_dfargments;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class how_all_cars extends Fragment {


    Button b1;
    public how_all_cars() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View my_view ;
        my_view = inflater.inflate(R.layout.fragment_how_all_cars,container,false);
        b1 = (Button)my_view.findViewById(R.id.show_cars);
        my_listner m1 = new my_listner();
        b1.setOnClickListener(m1);


        return my_view;
    }
    class my_listner implements View.OnClickListener
    {

        @Override
        public void onClick(View v) {
            MainActivity mn1 = (MainActivity)getActivity();
            String all="";
            for (int i=0;i<mn1.l1.size();i++)
            {
                car cr11;
                cr11 = mn1.l1.get(i);
                all = cr11.getCar_number()+"  "+cr11.getPrice();
            }


            b1.setText(all);
        }
    }

}
