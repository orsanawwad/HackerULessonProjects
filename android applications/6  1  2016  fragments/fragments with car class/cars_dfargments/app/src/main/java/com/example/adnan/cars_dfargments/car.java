package com.example.adnan.cars_dfargments;

/**
 * Created by adnan on 06/01/2016.
 */
public class car  {

    private String car_number;
    private int price;

    public car(String car_number, int price) {
        this.car_number = car_number;
        this.price = price;
    }

    public String getCar_number() {
        return car_number;
    }

    public void setCar_number(String car_number) {
        this.car_number = car_number;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
