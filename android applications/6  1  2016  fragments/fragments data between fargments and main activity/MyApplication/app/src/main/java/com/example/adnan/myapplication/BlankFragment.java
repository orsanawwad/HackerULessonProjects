package com.example.adnan.myapplication;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


/**
 * A simple {@link Fragment} subclass.
 */
public class BlankFragment extends Fragment {


    public BlankFragment() {
        // Required empty public constructor
    }

Button b1;
    EditText ed1;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v1 = inflater.inflate(R.layout.fragment_blank,container,false);
        b1 = (Button)v1.findViewById(R.id.button);
        my_listner m1 = new my_listner();
        b1.setOnClickListener(m1);
ed1 = (EditText)v1.findViewById(R.id.t1);
        return v1;
    }

    class my_listner implements View.OnClickListener
    {


        @Override
        public void onClick(View v) {

            MainActivity m1 = (MainActivity)getActivity();
            int num = Integer.parseInt(ed1.getText().toString());

            m1.l1.add(num);
        }
    }

}
