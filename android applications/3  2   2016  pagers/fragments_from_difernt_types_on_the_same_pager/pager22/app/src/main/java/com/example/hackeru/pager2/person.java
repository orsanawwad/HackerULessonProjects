package com.example.hackeru.pager2;

/**
 * Created by hackeru on 2/3/2016.
 */
public class person {

    String name;
    String id;

    @Override
    public String toString() {
        return "person{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                '}';
    }

    public person(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
