package com.example.hackeru.pager2;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class show_all_fragment extends Fragment {


    Button b1;
    public show_all_fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View my_view = inflater.inflate(R.layout.fragment_show_all_fragment,container,false);
        b1 = (Button)my_view.findViewById(R.id.btn1_fr_show);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MainActivity mc1 = (MainActivity)getActivity();

                List<person>  my_list;
                my_list = mc1.getMy_persons_list();
                String all="";
                for (int i=0;i<my_list.size();i++)
                    all += my_list.get(i)+"\n";

                b1.setText(all);
            }
        });



        return my_view;
    }

}
