package com.example.hackeru.pager2;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


/**
 * A simple {@link Fragment} subclass.
 */
public class add_person_fragment extends Fragment {


    Button b1;
    EditText ed_name,ed_id;
    public add_person_fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View my_view = inflater.inflate(R.layout.fragment_add_person_fragment,container,false);
        b1 = (Button)my_view.findViewById(R.id.button);
        ed_id= (EditText)my_view.findViewById(R.id.ed_id);
        ed_name = (EditText)my_view.findViewById(R.id.ed_name);

        my_listner m1 = new my_listner();
        b1.setOnClickListener(m1);

        return my_view;
    }

    public  class my_listner implements View.OnClickListener
    {


        @Override
        public void onClick(View v) {

            MainActivity mac1 = (MainActivity)getActivity();
            person p1 = new person(ed_name.getText().toString().trim(),ed_id.getText().toString().trim());
            ed_name.setText("");
            ed_id.setText("");
            mac1.getMy_persons_list().add(p1);
        }
    }
}
