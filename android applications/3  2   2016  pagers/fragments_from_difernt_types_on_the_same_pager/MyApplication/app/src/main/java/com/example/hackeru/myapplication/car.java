package com.example.hackeru.myapplication;

/**
 * Created by hackeru on 10/02/2016.
 */
public class car  {
    private String car_id;
    private String user_id;
    private String year;

    public car(String car_id, String user_id, String year) {
        this.car_id = car_id;
        this.user_id = user_id;
        this.year = year;
    }

    public car() {
    }

    public String getCar_id() {

        return car_id;

    }

    public void setCar_id(String car_id)throws my_ex{

        int i = car_id.length();

        if (i == 7)
        {
            for (int k = 0; k <= i; k++)
            {
               if (car_id.charAt(k) < 48 || car_id.charAt(k) >57)
               {
                   throw new my_ex("car id hawe laters or symbols",1);
               }
            }
        }
        else if (i < 7)
        {
            throw new my_ex("car id smolst at 7",3);
        }
        else
        {
            throw new my_ex("car id hiager at 7",2);
        }

        this.car_id = car_id;
    }

    public String getUser_id() {

        return user_id;
    }

    public void setUser_id(String user_id) throws my_ex{
        int i = Integer.parseInt(user_id);
        if (i < 9 || i > 9)
        {
            throw new my_ex("user id smolerst or higer at 9",4);
        }
        this.user_id = user_id;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
