package com.example.hackeru.servers;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {

    Button b1, b2;
    EditText ed_name, ed_code, ed_add, ed_price;
    AsyncHttpClient clinet5 = new AsyncHttpClient();
    RequestParams params = new RequestParams();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                b1 = (Button) findViewById(R.id.add);
                b2 = (Button) findViewById(R.id.show);
                ed_name = (EditText) findViewById(R.id.ed_name);
                ed_code = (EditText) findViewById(R.id.ed_code);
                ed_add = (EditText) findViewById(R.id.ed_add);
                ed_price = (EditText) findViewById(R.id.ed_price);
            }


        });
    }


    public void my_click(View V1) {


        switch (V1.getId()) {
            case R.id.add:

                params.put("name", ed_name.getText().toString());
                params.put("code", Integer.parseInt(ed_code.getText().toString()));
                params.put("added_by", ed_add.getText().toString());
                params.put("price", Integer.parseInt(ed_price.getText().toString()));

                clinet5.get("http://95.213.174.186/a1/home/get_all", new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        b1.setText(responseString);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        b1.setText(responseString);
                    }


                });
            case R.id.show:
                params.put("code", Integer.parseInt(ed_code.getText().toString()));
                clinet5.get("http://95.213.174.186/a1/home/find_by_code", params, new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        b2.setText(responseString);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                      b2.setText(responseString);
                    }
                });

        }


    }

}





















