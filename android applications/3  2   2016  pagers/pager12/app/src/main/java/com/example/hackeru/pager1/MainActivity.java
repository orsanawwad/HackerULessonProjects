package com.example.hackeru.pager1;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ViewPager v1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        List<first_fragment> my_list = create_fragments();
        my_pager m1 = new my_pager(getSupportFragmentManager(),my_list);

        v1 = (ViewPager)findViewById(R.id.v1);
        v1.setAdapter(m1);


    }

    public List<first_fragment>  create_fragments()
    {
        List<first_fragment> l1= new ArrayList<first_fragment>();
        first_fragment f1 = new first_fragment();
        l1.add(f1);
f1.setMe(" sonia ");
        first_fragment f2 = new first_fragment();
        l1.add(f2);
f2.setMe(" Bronshtien ");
        first_fragment f3 = new first_fragment();
        l1.add(f3);
f3.setMe(" programistit ");
        first_fragment f4 = new first_fragment();
        l1.add(f4);
        f4.setMe(" love doges ");
        return  l1;
    }


}
