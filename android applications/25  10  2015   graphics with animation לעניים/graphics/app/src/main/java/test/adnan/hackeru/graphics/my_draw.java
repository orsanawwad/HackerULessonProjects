package test.adnan.hackeru.graphics;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by hackeru on 25/10/2015.
 */
public class my_draw extends View {


    float cx=0,cy=0;
    public my_draw(Context context) {
        super(context);
        my_task m1 = new my_task();
        m1.execute(10);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Paint p1 = new Paint();
        p1.setColor(Color.RED);
        canvas.drawLine(100, 100, 300, 300, p1);
        canvas.drawCircle(cx,cy,50,p1);

    }
    public class my_task extends AsyncTask<Integer,Integer,Integer>
    {

        @Override
        protected Integer doInBackground(Integer... params) {

            while(cx<1000)
            {
                cx+=10;
                cy+=10;
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                publishProgress(10);
            }


            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            invalidate();
        }
    }

}
