package com.example.hackeru.animation1;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button b1 = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b1 = (Button)findViewById(R.id.btn_alpha);


    }
    public  void my_click(View v1)
    {
        switch (v1.getId())
        {
            case R.id.btn_alpha:
                ObjectAnimator ob1 = ObjectAnimator.ofFloat(b1,View.ALPHA,0.2f);
                ob1.setRepeatCount(10);
                ob1.setRepeatMode(ValueAnimator.RESTART);
                ob1.setDuration(1000);
                ob1.start();

                break;
            case R.id.btn_rotate:

                ObjectAnimator ob2 = ObjectAnimator.ofFloat(b1,View.ROTATION_X,360);
                ob2.setDuration(1000);
                ob2.setRepeatCount(ValueAnimator.INFINITE);
                ob2.setRepeatMode(ValueAnimator.RESTART);
                ob2.start();

                break;
            case R.id.btn_rotate_y:
                ObjectAnimator ob3 = ObjectAnimator.ofFloat(b1,View.ROTATION_Y,360);
                ob3.setRepeatMode(ValueAnimator.REVERSE);
                ob3.setDuration(1000);
                ob3.setRepeatCount(ValueAnimator.INFINITE);
                ob3.setInterpolator(new BounceInterpolator());
                ob3.start();
                break;
            case R.id.btn_scale_x:
                ObjectAnimator ob4 = ObjectAnimator.ofFloat(b1,View.SCALE_X,0,100);
                ob4.setRepeatMode(ValueAnimator.REVERSE);
                ob4.setDuration(1000);
                ob4.setRepeatCount(ValueAnimator.INFINITE);

                //   AccelerateInterpolator Rate of change starts out slowly and and then accelerates
//   BounceInterpolator    Change bounces right at the end
                //  DecelerateInterpolator    Rate of change starts out quickly and and then decelerates
                //  LinearInterpolator    Rate of change is constant throughout

                ob4.setInterpolator(new AccelerateInterpolator());
                ob4.start();
                break;

        }





    }
}
