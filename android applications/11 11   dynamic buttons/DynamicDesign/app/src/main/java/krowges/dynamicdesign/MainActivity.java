package krowges.dynamicdesign;


        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.view.Menu;
        import android.view.MenuItem;
        import android.view.View;
        import android.widget.Button;
        import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {

    LinearLayout ln1=null;
    Button b1,b2,b3;
    int i=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b1 = new Button(this);
        b2 = new Button(this);
        b3 = new Button(this);
        b1.setText("button1 ");
        b2.setText("button2 ");
        b3.setText("button3 ");

        ln1 = (LinearLayout)findViewById(R.id.lay1);

        b1.setId(i);
        i++;
        b2.setId(i);

        i++;
        b3.setId(i);

        my_listner m1 = new my_listner();
        b1.setOnClickListener(m1);

        b2.setOnClickListener(m1);

        b3.setOnClickListener(m1);

        ln1.addView(b1);
        ln1.addView(b2);
        ln1.addView(b3);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class my_listner implements View.OnClickListener
    {

        @Override
        public void onClick(View v) {

            switch (v.getId())
            {
                case 0:

                    b1.setText("button1 was clicked");
                    break;
                case 1:
                    b2.setText("button2 was clicked");
                    break;

                case 2:
                    b3.setText("button3 was clicked");
                    break;
            }
        }
    }
}