package test.adnan.hackeru.dynamic_table_layout;

import android.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;

public class MainActivity extends AppCompatActivity {


    LinearLayout ln1 = null;
    Button b1;
    Button b2 = null;
    TableLayout t1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            ln1 = new LinearLayout(this);


            t1 = new TableLayout(this);
            b1 = (Button)findViewById(R.id.button);

            LinearLayout.LayoutParams lpr = new LinearLayout.LayoutParams( ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

            t1.setLayoutParams(lpr);
            ln1.setLayoutParams(lpr);

            ln1.setOrientation(LinearLayout.HORIZONTAL);

            TableRow tr = new TableRow(this);
            Button b3,b4;
            b3 = new Button(this);
            b3.setText("b3");
            b4= new Button(this);
            b4.setText("b3");
            tr.addView(b3);
            tr.addView(b4);
            t1.addView(tr);




            TableRow tr2 = new TableRow(this);
            Button b5,b6;
            b5 = new Button(this);
            b5.setText("b3");
            b6= new Button(this);
            b6.setText("b3");
            tr.addView(b5);
            tr.addView(b6);
            t1.addView(tr2);





            b2 = new Button(this);
            b2.setText("hello i am at dynamic layout");
        //    ln1.addView(b2);
        } catch (Exception e) {
          b1.setText(e.getMessage());
        }

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ln1.addView(t1);
                setContentView(ln1);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
