package com19.example.hackeru.folders_adapters.adapters;


import  com19.example.hackeru.folders_adapters.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import com19.example.hackeru.folders_adapters.classes.user;

/**
 * Created by hackeru on 3/27/2016.
 */
public class my_adapter extends ArrayAdapter<user> {

    List<user> my_list;
    public my_adapter(Context context, int resource, List<user> objects) {
        super(context, resource, objects);
        my_list = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        user u1 = getItem(position);

        if(convertView== null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_layout,parent,false);
        }
        ImageView im1 = (ImageView)convertView.findViewById(R.id.im1);
        im1.setImageResource(u1.getPic());
        TextView t_name,t_phone;
        t_name = (TextView)convertView.findViewById(R.id.tv_name);
        t_phone = (TextView)convertView.findViewById(R.id.tv_phone);
        t_name.setText(u1.getName());
        t_phone.setText(u1.getPhone());



        return convertView;
    }
}
