package com19.example.hackeru.folders_adapters;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import com19.example.hackeru.folders_adapters.adapters.my_adapter;
import com19.example.hackeru.folders_adapters.classes.user;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    EditText ed_name,ed_phone;
    Button b1;
    ListView list_v1;
    my_adapter adapter1;
    List<user> l1 = new ArrayList<user>();
    int []iamges = {R.drawable.p1,R.drawable.p2,R.drawable.p3,R.drawable.p4} ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        user u1 = new user("aaaa","111",iamges[0]);
        l1.add(u1);
        user u2 = new user("bbbb","222",iamges[1]);
        l1.add(u2);
        adapter1 = new my_adapter(this,0,l1);
        list_v1 = (ListView)findViewById(R.id.my_list);
        list_v1.setAdapter(adapter1);
        user u3 = new user("cccc","222",iamges[2]);
        adapter1.add(u3);
        ed_name = (EditText)findViewById(R.id.ed_name);
        ed_phone = (EditText)findViewById(R.id.ed_phone);
        b1 = (Button)findViewById(R.id.b1);


    }

    @Override
    public void onClick(View v) {


        user my_user = new user(ed_name.getText().toString(),ed_phone.getText().toString(),iamges[3]);
        adapter1.add(my_user);
    }
}
