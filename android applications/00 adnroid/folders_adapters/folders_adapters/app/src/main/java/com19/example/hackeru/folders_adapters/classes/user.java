package com19.example.hackeru.folders_adapters.classes;

/**
 * Created by hackeru on 3/27/2016.
 */
public class user {
  private   String name;
    private  String phone;
    private  int pic;

    public user(String name, String phone, int pic) {
        this.name = name;
        this.phone = phone;
        this.pic = pic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getPic() {
        return pic;
    }

    public void setPic(int pic) {
        this.pic = pic;
    }
}
