package test.adnan.hackeru.graphics_with_animation;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.AsyncTask;
import android.view.Display;
import android.view.View;

/**
 * Created by hackeru on 28/10/2015.
 */
public class myDraw extends View {

int width,height;
    int step_x=10;
    public myDraw(Context context,int width,int height) {
        super(context);

        my_task m1 = new my_task() ;
		this.width= width;
        this.height = height;

        m1.execute(200,0,100);


    }

    int cx=0,cy=0;
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Paint p1 = new Paint();
        p1.setColor(Color.GREEN);
        canvas.drawCircle(0, 200, 50, p1);

        canvas.drawCircle(cx,cy,50,p1);
    }

    public class my_task extends AsyncTask<Integer,Integer,Integer>
    {
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

          invalidate();

        }

        @Override
        protected Integer doInBackground(Integer... values) {

            int speed,max_x;
            speed = values[0];
            cx = values[1];
            cy = values[2];

            if(speed>0)
            while(true)
            {
                if(cx >= width)
                    step_x*=-1;
                if(cx<0)
                    step_x*=-1;

                cx+=step_x;

                try {
                    Thread.sleep(speed);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(speed>10)
                speed-=1;
                publishProgress(cx);
            }
            return null;
        }
    }

}
