package block_constractor;

public class person {

	private String name;
	private String id;
	private String city;
	
	
	{
		System.out.println("hello i am ur first constractor block");
		city = "haifa";
	}
	
	
	public person()
	{
		
	}
	
	{
		System.out.println("second block");
	}
	
	
	public person(String name, String id) {
		super();
		this.name = name;
		this.id = id;
	}


	public person(String name) {
		super();
		this.name = name;
	}


	public String getName() {
		return name;
	}
	
	{
		System.out.println("thered block");
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	

}
