package test.adnan.hackeru.calc_few_buttons_the_same_class;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    Button add,sub,mult,devide,res;
    EditText ed1,ed2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        add = (Button)findViewById(R.id.add);
        sub = (Button)findViewById(R.id.sub);
        mult = (Button)findViewById(R.id.mult);
        devide = (Button)findViewById(R.id.devide);
        res = (Button)findViewById(R.id.res);
        ed1 = (EditText)findViewById(R.id.editText);
        ed2 = (EditText)findViewById(R.id.editText2);

        my_listner m1 = new my_listner();
        add.setOnClickListener(m1);
        mult.setOnClickListener(m1);
        devide.setOnClickListener(m1);
        sub.setOnClickListener(m1);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar
        // {will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

     class  my_listner implements View.OnClickListener
    {

        @Override
        public void onClick(View v) {
            String s1,s2;
        int num1,num2,res1=0;
            s1 = ed1.getText().toString();
            s2 = ed2.getText().toString();
            num1 = Integer.parseInt(s1);
            num2 = Integer.parseInt(s2);
            switch ((v.getId()))
            {



                case R.id.add:
                    res1 = num1+num2;

                    break;
                case R.id.mult:
                    res1 = num1*num2;

                    break;
                case R.id.devide:
                    res1 = num1/num2;

                    break;
                case R.id.sub:
                    res1 = num1-num2;

                    break;
            }
            res.setText(res1+"");
        }

    }
}
