package com.example.hackeru.small_calc1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    Button b1,b2;
    EditText e1,e2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b1 = (Button)findViewById(R.id.b1);

        b2 = (Button)findViewById(R.id.b2);

        e1 = (EditText)findViewById(R.id.t1);
        e2 = (EditText)findViewById(R.id.t2);

        my_listnre m1 = new my_listnre();
        b1.setOnClickListener(m1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class my_listnre implements View.OnClickListener
    {

        @Override
        public void onClick(View v) {

            String s1,s2;
            int n1,n2,res;
            s1 = e1.getText().toString();
            s2 = e2.getText().toString();
            n1 = Integer.parseInt(s1);
            n2 = Integer.parseInt(s2);
            res = n1+n2;
            b2.setText(res+"");







        }
    }
}
