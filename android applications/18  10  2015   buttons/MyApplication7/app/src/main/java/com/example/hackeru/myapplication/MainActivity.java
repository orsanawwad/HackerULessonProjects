package com.example.hackeru.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button b1= null,b2=null,b3=null;
    int num_of_clickes=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // get the refrance of the button by the function
        // getviewbyid
        // we need casting ... the function rwturn type of class wich all inherate from
        b1 = (Button) findViewById(R.id.b1);
        b2 = (Button)findViewById(R.id.b2);
        b3 = (Button)findViewById(R.id.b3);
        b1.setText("hello i am ur first application");

    my_listner m1 = new my_listner();
        b1.setOnClickListener(m1);
        b2.setOnClickListener(m1);
        b3.setOnClickListener(m1);


        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public  class  my_listner implements View.OnClickListener
    {

        @Override
        public void onClick(View v) {

            switch (v.getId())
            {
                case R.id.b1:

                    b1.setText("u clicked me");
                    break;
                case R.id.b2:
                    b2.setText("u clicked me");
                    break;
                case R.id.b3:
                    b3.setText("u clicked me");
                    break;

            }




          //  num_of_clickes++;
          //  b1.setText("hello u clicked me"+num_of_clickes);
           // b2.setText("b1 was clicked");
            //b3.setText("b1 was clicked");
        }
    }
}
