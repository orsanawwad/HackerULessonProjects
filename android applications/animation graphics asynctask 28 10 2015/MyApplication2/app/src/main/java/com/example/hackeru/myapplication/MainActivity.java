package com.example.hackeru.myapplication;

import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int width, height;
        Display ds1 = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        ds1.getSize(size);
        width = size.x;
        height = size.y;

        my_draw m1 = new my_draw(this, width, height);

        setContentView(m1);
    }
}
