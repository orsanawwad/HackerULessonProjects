package test.adnan.hackeru.save_pic_in_sqlite;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;

public class MainActivity extends AppCompatActivity {

    EditText ed1 = null;
    Button b1,b2;
    ImageView im1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        ed1 = (EditText)findViewById(R.id.editText);
        b1 = (Button)findViewById(R.id.save);
        b2 = (Button)findViewById(R.id.get);
        im1 = (ImageView)findViewById(R.id.im1);
        my_listner m1 = new my_listner();
        b1.setOnClickListener(m1);
        b2.setOnClickListener(m1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Bundle extra = data.getExtras();
        Bitmap btmp1 = (Bitmap)extra.get("data");
        byte []image;
        image = convert_bitmap_to_byte(btmp1);
        my_db md1 = new my_db(getApplicationContext(),"db11",null,1);
        int code;
        code = Integer.parseInt(ed1.getText().toString());

        md1.add_to_table(code,image);


    }
    public  byte[]convert_bitmap_to_byte(Bitmap btmp1)
    {
        byte []array=null;
        ByteArrayOutputStream stream =  new ByteArrayOutputStream();
        btmp1.compress(Bitmap.CompressFormat.PNG,0,stream);
        array = stream.toByteArray();

        return  array;
    }
    public Bitmap convert_byte_array_to_bitmap(byte []array)
    {
        Bitmap res;
        res = BitmapFactory.decodeByteArray(array,0,array.length);
        return  res;
    }

    public  class  my_listner  implements View.OnClickListener
    {


        @Override
        public void onClick(View v) {

            switch (v.getId())
            {
                case R.id.save:

                    Intent in1 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(in1,1);


                    break;

                case R.id.get   :
                    my_db md1 = new my_db(getApplicationContext(),"db11",null,1);
                    byte []iamge;
                    Bitmap btmp1;
                    int code;
                    code = Integer.parseInt(ed1.getText().toString());
                    iamge = md1.get_image_by_code(code);
                    btmp1 = convert_byte_array_to_bitmap(iamge);
                    im1.setImageBitmap(btmp1);

                    break;
            }
        }
    }


}
