package test.adnan.hackeru.my_db2;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

/**
 * Created by hackeru on 08/11/2015.
 */
public class persons  extends SQLiteOpenHelper {
    Context cn;
    public persons(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        cn = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String query;
        query = "create table if not exists t1(num integer)";
        db.execSQL(query);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        SQLiteDatabase db1 = getWritableDatabase();
        Toast.makeText(cn,"on upgrade called",Toast.LENGTH_LONG).show();

       
        String query;
        query = "alter table t1 add coloumn num2 integer";
        db1.execSQL(query);
        this.onCreate(db1);



    }
    public void add(int x)
    {
        ContentValues cn= new ContentValues();
        cn.put("num", x);
        SQLiteDatabase db = getWritableDatabase();
        db.insert("t1",null,cn);


    }
}
