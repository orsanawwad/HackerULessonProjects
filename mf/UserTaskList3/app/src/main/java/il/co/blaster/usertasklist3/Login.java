package il.co.blaster.usertasklist3;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {

	EditText _myUser, _myPass;
	Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		_myUser = (EditText) findViewById(R.id.myUser);
		_myPass = (EditText) findViewById(R.id.myPass);
		context = this;

		Globals.getInstance().setCurrentContext(context);
		boolean checkLogin = PrefMan.getInstance().checkLoggedIn();
		System.out.println("********************** " + checkLogin);
		if (checkLogin){
			Intent goToTasks = new Intent(context, TaskList.class);
			context.startActivity(goToTasks);
			finish();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		context = this;
		Globals.getInstance().setCurrentContext(context);
		if (PrefMan.getInstance().checkLoggedIn()) {
			Intent goToTasks = new Intent(context, TaskList.class);
			context.startActivity(goToTasks);
			finish();
		}
		;
	}


	public void FLogin(View view) {
		String _myUserTxt = _myUser.getText().toString();
		String _myPassTxt = _myPass.getText().toString();
		if (PrefMan.validateUser(_myUserTxt, _myPassTxt)) {
			Intent goToTasks = new Intent(context, TaskList.class);
			context.startActivity(goToTasks);
			finish();
		}
	}

	public void FRegister(View view) {
		Intent goToRegister = new Intent(context, Register.class);
		context.startActivity(goToRegister);
		finish();
	}
}



