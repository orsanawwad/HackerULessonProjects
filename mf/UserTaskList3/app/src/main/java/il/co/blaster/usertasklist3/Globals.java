package il.co.blaster.usertasklist3;

import android.content.Context;

public class Globals {

	//init singleton

	private static Globals ourInstance = new Globals();

	public static Globals getInstance() {
		return ourInstance;
	}

	private Globals() {
	}

	//declarations

	public static final boolean MYDBGMODE = true; // debug mode


	static Context currentContext;

	public static void setCurrentContext(Context context) {
		currentContext = context;

	}

	public static Context getCurrentContext() {
		return currentContext;
	}


}

