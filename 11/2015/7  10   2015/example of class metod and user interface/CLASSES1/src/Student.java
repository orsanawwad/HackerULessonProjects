
public class Student {
	
	
	public String name;
	public String id;
	public float avrg;
	private int num_of_examps;
	
	public void add_examps(int x)
	{
		this.num_of_examps +=x;
	}
	
	
	
	public int getNum_of_examps() {
		return num_of_examps;
	}
	public Student(String name, String id, float avrg) {
		//super();
		this.name = name;
		this.id = id;
		this.avrg = avrg;
	}
	public Student(String name, String id) {
		super();
		this.name = name;
		this.id = id;
	}
	public Student() {
		super();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getAvrg() {
		return avrg;
	}
	public void setAvrg(float avrg) {
		this.avrg = avrg;
	}
	public String getId() {
		return id;
	}
	

}
