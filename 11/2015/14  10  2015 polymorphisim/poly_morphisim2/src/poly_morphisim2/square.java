package poly_morphisim2;

public class square {
	
	
	
	public String toString() {
		return "square [len=" + len + "]";
	}
	protected int len;

	public int getLen() {
		return len;
	}

	public void setLen(int len) {
		this.len = len;
	}

	public square(int len) {
		super();
		this.len = len;
	}
	public double shetah()
	{
		return this.len*this.len;
	}
	public double hekef()
	{
		return 4*this.len;
	}

}
